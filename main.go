package main

import (
	"crypto/md5"
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"regexp"
	"text/template"
	"time"

	"golang.org/x/exp/inotify"

	"github.com/gorilla/websocket"
)

const (
	// Time allowed to write the file to the client.
	writeWait = 10 * time.Second

	// Time allowed to read the next pong message from the client.
	pongWait = 60 * time.Second

	// Send pings to client with this period. Must be less than pongWait.
	pingPeriod = (pongWait * 9) / 10

	// Poll file for changes with this period.
	filePeriod = 10 * time.Second

	imageHTTPPath = "/image/"
)

var (
	addr      = flag.String("addr", "127.0.0.1:8080", "http service address")
	homeTempl = template.Must(template.New("").Parse(homeHTML))

	imageDir  = flag.String("dir", "some_images", "directory with images (need write access!)")
	imgRegExp = regexp.MustCompile(`\.(gif|jpe?g|png)$`)
	upgrader  = websocket.Upgrader{
		ReadBufferSize:  1024,
		WriteBufferSize: 1024,
	}
)

type img struct {
	UniqID string `json:"uniqid"`
	SRC    string `json:"src"`
	//TODO add some another fields look like width heght etc
}

type wsResponse struct {
	Success      bool   `json:"success"`
	ErrorMessage string `json:"error_message"`
	Images       []img  `json:"images"`
}

func readDirImgs() ([]img, error) {
	fis, err := ioutil.ReadDir(*imageDir)

	if err != nil {
		return nil, err
	}
	res := []img{}
	for _, x := range fis {
		if isPic(x) {
			res = append(res, newImgFromFileInfo(x))
		}
	}
	return res, nil

}

func isChangedEvent(ev *inotify.Event) bool {
	return ev.Mask&inotify.IN_CREATE > 0 ||
		ev.Mask&inotify.IN_DELETE > 0 ||
		ev.Mask&inotify.IN_DELETE_SELF > 0 ||
		ev.Mask&inotify.IN_MOVE > 0 ||
		ev.Mask&inotify.IN_MOVE_SELF > 0 ||
		ev.Mask&inotify.IN_MOVED_FROM > 0 ||
		ev.Mask&inotify.IN_MOVED_TO > 0
}

func newImgFromFileInfo(fi os.FileInfo) img {
	return img{
		SRC:    imageHTTPPath + fi.Name(),
		UniqID: "x" + fmt.Sprintf("%x", md5.Sum([]byte(fi.Name()))),
	}
}

func isPic(fi os.FileInfo) bool {
	if fi.IsDir() {
		return false
	}

	return imgRegExp.Match([]byte(fi.Name()))
}

func reader(ws *websocket.Conn) {
	defer ws.Close()
	ws.SetReadLimit(512)
	ws.SetReadDeadline(time.Now().Add(pongWait))
	ws.SetPongHandler(func(string) error { ws.SetReadDeadline(time.Now().Add(pongWait)); return nil })
	for {
		_, _, err := ws.ReadMessage()
		if err != nil {
			break
		}
	}
}

func writer(ws *websocket.Conn) {
	lastError := ""
	pingTicker := time.NewTicker(pingPeriod)
	watcher, err := inotify.NewWatcher()
	if err != nil {
		log.Fatal(err)
		return
	}

	err = watcher.Watch(*imageDir)

	if err != nil {
		log.Fatal(err)
		return
	}

	defer func() {
		pingTicker.Stop()
		watcher.Close()
		ws.Close()
	}()
	for {
		select {
		case ev := <-watcher.Event:
			var res wsResponse
			if !isChangedEvent(ev) {
				continue
			}
			log.Printf("evt = %v\n", ev)
			res.Images, err = readDirImgs()
			if err != nil {
				if s := err.Error(); s != lastError {
					lastError = s
					res.Success = false
					res.ErrorMessage = lastError
				}
			} else {
				lastError = ""
				res.Success = true
			}

			ws.SetWriteDeadline(time.Now().Add(writeWait))
			if err := ws.WriteJSON(res); err != nil {
				return
			}

		case <-pingTicker.C:
			ws.SetWriteDeadline(time.Now().Add(writeWait))
			if err := ws.WriteMessage(websocket.PingMessage, []byte{}); err != nil {
				return
			}
		}
	}
}

func serveWs(w http.ResponseWriter, r *http.Request) {
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		if _, ok := err.(websocket.HandshakeError); !ok {
			log.Println(err)
		}
		return
	}

	go writer(ws)
	reader(ws)
}

func serveHome(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.Error(w, "Not found", 404)
		return
	}
	if r.Method != "GET" {
		upload(w, r)
	}
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	images, err := readDirImgs()
	if err != nil {
		log.Printf("smth happen: %v\n", err)
	}
	imagesJSON, _ := json.MarshalIndent(images, "", "    ")
	var v = struct {
		Host   string
		Images string
	}{
		r.Host,
		string(imagesJSON),
	}

	homeTempl.Execute(w, &v)
}

// upload logic
func upload(w http.ResponseWriter, r *http.Request) {
	r.ParseMultipartForm(32 << 20)
	file, handler, err := r.FormFile("uploadfile")
	if err != nil {
		fmt.Println(err)
		return
	}
	defer file.Close()

	if !imgRegExp.Match([]byte(handler.Filename)) {
		fmt.Printf("image [%v] already existed!\n", handler.Filename)
		return
	}
	f, err := os.OpenFile(*imageDir+`/`+handler.Filename, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer f.Close()
	io.Copy(f, file)
}

func main() {
	flag.Parse()

	fmt.Printf("started with imageDir=%s\nopen %s on browser/\n", *imageDir, *addr)
	http.HandleFunc("/", serveHome)
	http.HandleFunc("/ws", serveWs)
	http.Handle(imageHTTPPath, http.StripPrefix(imageHTTPPath, http.FileServer(http.Dir(*imageDir))))
	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal(err)
	}
}

const homeHTML = `<!DOCTYPE html>
<html lang="en">
<head>
    <title>WebSocket Example</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
    <style>
    .gallery img{
    	width: 150px;
    	max-height: 150px;
    	margin: 10px;
    </style>
</head>
<body>
<form enctype="multipart/form-data" action="" method="post">
      <input type="file" name="uploadfile" />
      <input type="submit" value="upload" />
</form>
<div class="gallery"></div>
<script type="text/javascript">
    $(function() {
		function fillGallery(images) {
			$('img', $gallery).hide()
			for(var i=0; i < images.length; i++) {
				var $img = $('#x'+images[i].uniqid);
				if ($img.length) {
					$img.show();
					continue
				}
				var $newImg = $('<img />');
				$newImg.attr("src", images[i].src)
				$newImg.attr("id", images[i].uniqid);
				$newImg.appendTo($gallery);
			}
		}
		var $gallery = $('.gallery'),
        	conn = new WebSocket("ws://{{.Host}}/ws");

        fillGallery({{.Images}})

        conn.onclose = function(evt) {
            data.textContent = 'Connection closed';
        }
        conn.onmessage = function(evt) {
            var wsResp = JSON.parse(evt.data)
            if (wsResp.images) {
                fillGallery(wsResp.images);
            }
        }
    });
</script>
</body>
</html>
`
